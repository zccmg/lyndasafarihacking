var webPage = require('webpage');
var Async =  require("async");
var fs = require("fs");
var _ =  require("lodash");
var page = webPage.create();
var Util = require("./util");

var data = {
    pages: []
};

var isFormSubmitted = false;

page.onLoadFinished = function () {
    if(isFormSubmitted) {
        isFormSubmitted = false;
        console.log("reached 1");
        setTimeout(loadPages, 500);
    }
};
page.settings.loadImages = false;
page.onConsoleMessage = function(msg) {
    console.log("CONSOLE: ", msg);
};
page.open("https://www.safaribooksonline.com/library/view/building-microservices/9781491950340", function (status) {
    var number = 0;
    if(status == "success") {
        data.title = Util.saveName(page.evaluate(function () {
            return document.querySelector("h1.t-title").textContent
        }));

        data.pages.push({
            number: number++,
            url: page.url + "/cover.html"
        });

        data.pages = data.pages.concat(page.evaluate(function (number) {
            var pages = [];

            var elms = document.querySelectorAll("li.toc-level-1.t-toc-level-1");

            for(var i =0; i < elms.length; i++) {
                pages.push({
                    number: number++,
                    url: elms[i].querySelector("a.t-chapter").href
                })
            }
            return pages;
        }, number));

        setTimeout(login, 500);
    } else phantom.exit(1);
});


function login() {
    page.open('https://www.safaribooksonline.com/accounts/login/', function(status) {
        console.log("Status: " + status);
        if(status === "success") {
            page.evaluate(function () {
                document.querySelector("#id_email").value = "ahmedsafari";
                document.querySelector("#id_password1").value = "S@f@r1b00ks";
                document.querySelector("form").submit()
            });
            isFormSubmitted = true;
        } else phantom.exit(1)
    });
}

function loadPages() {
    var tempFolder = "./.tmp/" + data.title + "/";
    fs.makeTree(tempFolder);
    Async.eachOfSeries(data.pages, function (bookPage, key, next) {
        page.open(bookPage.url, function (status) {
            console.log("Page Book Loaded", status, bookPage.number);
            if(status === "success") {
                var res = page.evaluate(function (number, css) {
                    var styleTag = document.createElement("style");
                    styleTag.textContent = css;
                    var contentElm = document.querySelector("#sbo-rt-content .annotator-wrapper");
                    var rmElms = contentElm.querySelectorAll('*[class^="annotator"]');
                    for(var elIndex =0; elIndex < rmElms.length; elIndex++) {
                        rmElms[elIndex].remove();
                    }
                    document.head.innerHTML = "";
                    document.body.innerHTML = "";
                    document.head.appendChild(styleTag);
                    document.body.appendChild(contentElm);
                    var footer = document.createElement("footer");
                    footer.textContent = number;
                    document.body.appendChild(footer);
                    var D = document;
                    return {
                        html: document.documentElement.innerHTML,
                        height: Math.max(contentElm.scrollHeight,
                            D.body.scrollHeight, D.documentElement.scrollHeight,
                            D.body.offsetHeight, D.documentElement.offsetHeight,
                            D.body.clientHeight, D.documentElement.clientHeight
                        )
                    };
                }, bookPage.number, fs.read("./bookStyle.css"));
                var tempFile = tempFolder + bookPage.number + ".html";
                fs.write(tempFile, res.html, "w");
                data.pages[key] = {
                    url: tempFile,
                    height: res.height
                }
                console.log("Page End", bookPage.number);
                setTimeout(next, 500);
            } else next("Status: " + status)
        });
    }, function (err) {
        console.log(err);
        fs.write("completedBook.json", JSON.stringify(data), "w");
        phantom.exit(0)
    });
}
