
var mkdirp = require("mkdirp");
var Fs = require("fs");
var URI = require("uri-js");
var Async = require("async");
var Path = require("path");
var _ = require("lodash");
var Util = require("./util");

var data = JSON.parse(Fs.readFileSync(__dirname + "/completeSafari.json").toString());
var parentPath = "H:/Gradle for Android";


Async.each(data, function (group, done) {
    var groupPath = Path.join(parentPath, group.id + ". " + Util.saveName(group.groupTitle));

    Async.series([
        Async.apply(mkdirp, groupPath),
        function (next) {
            Async.eachLimit(group.courses, 1, function (course, finish) {
                var task = {
                    path: Path.join(groupPath, course.id + ". " + Util.saveName(course.name) + Path.extname(course.video.split("?")[0])),
                    url: URI.serialize(URI.parse(course.video))
                };

                if(Fs.existsSync(task.path) && !Fs.existsSync(task.path + ".aria2")) {
                    return finish();
                }
                var exec = require('child_process').execFile;
                var url = task.url.split("?")[0] + "?referrer=aHR0cHM6Ly93d3cuc2FmYXJpYm9va3NvbmxpbmUuY29t&ks=djJ8MTkyNjA4MXyTH_Pk7DbonvX0_cOCB9XS1YLFZMhEllUm79lKgw3fZ9-RpmNk7iUjJZAxJc9w-H4Z-utkt_kM7OXE8wUZ7fNjVamyMt46YjUz2whAivxPjw==&playSessionId=48dd12d3-ac4a-a5f0-7654-58251ce52792&clientTag=html5:v2.42&uiConfId=29375172";
                var appPath = Path.join(__dirname, "aria2/aria2c.exe");
                var call = exec(appPath, [
                    "-x8",
                    "-l logs.txt",
                    "-d " + Path.dirname(task.path),
                    "-o " + Path.basename(task.path),
                    url
                ], function(error, stdout, stderr) {
                    if (error !== null) {
                        console.log('exec error: ', error);
                    }
                    finish();
                });
                call.stdout.pipe(process.stdout);
                call.stderr.pipe(process.stderr);
            }, next)
        }
    ], done)
}, function (err) {
    console.log("All data added to the queue");
});
