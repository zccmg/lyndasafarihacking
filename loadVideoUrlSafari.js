var page = require("webpage").create();
var fs = require("fs");
var Async =  require("./node_modules/async/dist/async.js");
var _ = require("./node_modules/lodash/lodash");

var data = JSON.parse(fs.read("responseSafari.json"));

var isFormsubmited = false;
page.viewportSize = { width: 80, height: 80 };
page.onResourceReceived = function(response) {
    if(_.includes(response.url, "https://cdnapisec.kaltura.com/p/") && response.stage == "end") {
        for(var i=0; i<data.length; i++) {
            var founded = false;
            _.forEach(data[i].courses, function (course, j) {
                if(_.isEqual(course.url, response.referrer)) {
                    data[i].courses[j].video = response.url;
                    founded = true;
                    return false;
                }
            });
            if(founded) break;
        }
    } else if(_.includes(response.url, "https://cdnsecakmi.kaltura.com/api_v3/index.php/service/caption_captionAsset/action/serve/captionAssetId") && response.stage == "end") {
        for(var i=0; i<data.length; i++) {
            founded = false;
            _.forEach(data[i].courses, function (course, j) {
                if(_.isEqual(course.url, response.referrer)) {
                    data[i].courses[j].subtitle = response.url;
                    founded = true;
                    return false;
                }
            });
            if(founded) break;
        }
    }
};

page.onLoadFinished = function () {
    if(isFormsubmited) {
        isFormsubmited = false;
        console.log("reached 1");
        setTimeout(loadVideos, 500);
    }
};

page.open('https://www.safaribooksonline.com/accounts/login/').then(function(status) {
    console.log("Status: " + status);
    if(status === "success") {
        page.evaluate(function () {
            document.querySelector("#id_email").value = "ahmedsafari";
            document.querySelector("#id_password1").value = "S@f@r1b00ks";
            document.querySelector("form").submit()
        })
        isFormsubmited = true;
    }
});

function loadVideos() {
    Async.eachSeries(data, function (group, done) {
        Async.eachSeries(group.courses, function (course, next) {
            page.open(course.url).then(function () {
                page.close();
                next(null)
            })
        }, done)
    }, function () {
        fs.write("completeSafari.json", JSON.stringify(data), "w");
        phantom.exit();
    })
}









// page.settings.loadImages = true;
// page.onConsoleMessage = function(msg) {
//     console.log("CONSOLE: ", msg);
// }
//
//
//
// page.onInitialized = function () {
//     page.evaluate(function () {
//         var create = document.createElement;
//         document.createElement = function (tag) {
//             var elem = create.call(document, tag);
//             if (tag === "video") {
//                 elem.canPlayType = function () { return "probably" };
//             }
//             return elem;
//         };
//     });
// };
// page.onResourceError = function(resourceError) {
//     console.error(resourceError.url + ': ' + resourceError.errorString);
// };
//
// page.open(data[0].courses[0].url, function (status) {
//     console.log(status);
//     if(status === "success") {
//         setTimeout(function () {
//             fs.write("test.html", page.content, "w");
//             var elements = page.evaluate(function () {
//                 console.log(document.getElementsByTagName("video").length);
//                 return {
//                     subtitle: document.querySelector("track[kind='subtitle']").textContent,
//                     video: document.querySelector("video").getAttribute("src")
//                 }
//             });
//             console.log(elements);
//         }, 15000)
//     } else {
//         console.log("Failed", course.url);
//     }
// });

