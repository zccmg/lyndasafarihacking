var webPage = require('webpage');
var Async =  require("async");
var fs = require("fs");
var _ =  require("lodash");
var page = webPage.create();


page.settings.loadImages = false;
page.onConsoleMessage = function(msg) {
  console.log("CONSOLE: ", msg);
}


page.open("https://www.safaribooksonline.com/library/view/gradle-for-android/9781491941102/", function (status) {
  console.log("course accessed", status);

  Async.waterfall([
    function (finish) {
      var response = page.evaluate(function () {
        var title = "";
        var data = [];
        var idCounter = 0;
        var idGroupCounter = 0;

        function LoadUrl(title, elements) {
          var dat = {};
          dat.groupTitle = title;
          dat.id = idGroupCounter++;
          dat.courses = [];
          for(var j=0; j < elements.length; j++) {
            var courseElm = elements[j];
            var a = courseElm.querySelector("a");
            console.log("reached 6-", a.textContent);
            if (a) {
              dat.courses.push({
                url: "https://www.safaribooksonline.com" + a.getAttribute("href"),
                name: a.textContent,
                id: idCounter++
              })
            }
          }
          return dat;
        }

        var topicGroupsElm = document.querySelectorAll("ol.detail-toc li.toc-level-1");
        console.log(topicGroupsElm.length);
        if(_.first(topicGroupsElm).querySelector("ol") !== null) {
          for(var i=0; i< topicGroupsElm.length; i++) {
            var groupElm = topicGroupsElm[i];
            title = groupElm.querySelector("a.t-chapter").textContent;
            var coursesElm = groupElm.querySelectorAll("ol li.toc-level-2.t-toc-level-2") || [];
            var dat = LoadUrl(title, coursesElm);
            console.log("reached 5-", coursesElm.length);

            console.log("reached 7-", idCounter, JSON.stringify(dat));
            data.push(dat);
          }
        } else {
          title = document.querySelector("h1.t-title").textContent;
          data.push(LoadUrl(title, topicGroupsElm))
        }

        return data;
      });
      finish(null, response);
    }
  ], function (err, result) {
    if(err) {
      console.log(err);
      phantom.exit();
    } else {
      try {
        fs.write("responseSafari.json", JSON.stringify(result), "w");
        phantom.exit();
      } catch (ex) {
        console.log(ex);
      }
    }
  })

})
